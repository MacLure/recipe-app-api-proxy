# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

- `LISTEN_PORT` - Port to listen on (default: `0000`)
- `APP_HOST` - Hostname o fthe app to forward request to (default: `app`)
- `APP_PORT` - port of the app to forward the request to (default: `9000`)
